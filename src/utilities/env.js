export const getHostName = () => {
    if (typeof window !== "undefined") {
        return window.location.hostname
    }
    return process.env.hostname
}

export const getEnv = () => {
    if (getHostName().indexOf(".test.paas.eniro") > -1) {
       return "test"
    }
    return "prod"
}