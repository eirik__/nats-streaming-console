import openSocket from 'socket.io-client'
import { getHostName } from "../utilities/env";

const socket = openSocket('http://'+getHostName())

export const subscribeToChannel = (channel, opts, cb) => {
  socket.on(channel, message => cb(message))
  socket.emit('subscribe-to-channel', { channel, opts })
}
